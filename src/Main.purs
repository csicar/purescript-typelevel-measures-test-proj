module Main where

import Prelude
import Effect (Effect)
import Effect.Console (log)

import Type.Data.Units
import Type.Data.Units.SI
import Type.Data.Peano.Int

main :: Effect Unit
main = do
  log "Hello sailor!"


distance :: Int : Meter' ()
distance = liftV 10 ** meter


avgSpeed :: Int : Meter' () -> Int : Sec' () -> Int : Meter' * Sec N1 * ()
avgSpeed a b = a // b

speedOver10m :: Int : Meter' * Sec N1 ()
speedOver10m = avgSpeed distance (liftV 5 ** sec)

energyInBarOfChocolate :: Int : Joule ()
energyInBarOfChocolate = liftV 2_300_000 ** joule

forceOver5Meter :: Int : Newton ()
forceOver5Meter = energyInBarOfChocolate // (liftV 5 ** meter)



-- add Measured:
sum = speedOver10m ++ liftV 2 ** meter // sec